import json
import time
import requests
import logging
import os
from datetime import datetime


# Formatted date.
def getFormattedDate() -> str:
    now = datetime.now()
    return now.strftime("%d-%m-%Y-%H-%M-%S")


# Formatted time.
def getFormattedTime() -> str:
    now = datetime.now()
    return now.strftime("%H:%M:%S")


# getSimpleDate.
def getDateTime() -> str:
    return str(datetime.now())


# getLogging.
def getLogger():
    logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.DEBUG)
    return logging.getLogger()


#Getting the value of environment variables.
virtual_users = os.getenv("virtual_users")
instances = os.getenv("instances")
rampup_secs = os.getenv("rampup_secs")
duration_secs = os.getenv("duration_secs")
test_scenario_file_name = os.getenv("TEST_SCENARIO_FILE_NAME")
test_scenario_users_file = os.getenv("TEST_SCENARIO_USERS_FILE_NAME")
artifactDir = os.getenv("ARTIFACT_DIR")
authKey = os.getenv("authKey")
test_runner = os.getenv("TEST_RUNNER_SOURCE")


# TESTABLE_KEY
def getTestableKey():
    """
    All API requests must contain an API key for authentication. Passing API key to start test run.
    :return: params
    """
    params = {
        'key': authKey
    }
    return params


# HEADER_RESULT_KEY
def getHeaderResultKey() -> dict:
    """
    All API requests must contain an API key for authentication.
    :return: dicHeader
    """
    dicHeader = {
        'X-Testable-Key': authKey
    }
    return dicHeader

# JMeter test scenario files.
def getTestScript():
    getJmxScenarioScript = {
        'jmeter_testplan': open('./'+artifactDir+'/'+test_scenario_file_name, 'rb'),
        'files[0]': open('./'+artifactDir+'/'+test_scenario_users_file, 'rb')
    }
    return getJmxScenarioScript


# JMeter test plan sample parameters for Your AWS Account.
def getJmxAWSData():
    getLogger().info(f'Value of env variables inside script: Virtual_users:{virtual_users} Instances:{instances} Rampup_secs:{rampup_secs} Duration_secs: {duration_secs} TestScenarioFileName: {test_scenario_file_name}')
    jmxTestConfParams = {
        'params[threads]': virtual_users,
        'instances': instances,
        'params[rampup]': rampup_secs,
        'params[duration]':duration_secs,
        'testcase_name': 'MyTest_'+getFormattedTime(),
        'conf_testrunners[0].name': test_runner,
        'conf_testrunners[0].tags[Name]': 'Testable-Instance'
        #'kpis[0].expr': (None, 'Outcome[success] > 95%'),
        #'kpis[1].expr': (None, 'Response Time[Median] < 300ms'),
        #'conf_testrunners[0].aws_role_arn': (None, 'arn:aws:iam::165446266030:role/Cross-Account-Role-For-Testable'),
        #'conf_testrunners[0].regions[0].name': (None, 'us-east-1'),
        #'conf_testrunners[0].regions[0].vpc': (None, 'vpc-041296ed11f74987e'),
        #'conf_testrunners[0].regions[0].subnet': (None, 'subnet-0a1909544c9b22980')
    }
    return jmxTestConfParams


# Get URL
def getUrl(endPoint) -> str:
    baseURL = 'https://api.testable.io' + endPoint
    return baseURL


# OutputURL.
def getOutputURL(resID) -> str:
    return 'https://a.testable.io/results/'+resID


# public URL.
def getPublicURL() -> str:
    return 'https://a.testable.io'


# To start test run.
def start_Test(endPoint_start):
    # endPoint_start = endPoint_start  #'/start'
    try:
        response = requests.post(url=getUrl(endPoint_start), headers=getHeaderResultKey(), data=getJmxAWSData(), files=getTestScript())
        getLogger().info(f'Response Status Code: {response.status_code} and Response Content: {response.content}')
        if response.status_code == 200:
            getLogger().info(f'Test case started, ResponseID: {response.text}')
            checkTestStatus(response.text)
        else:
            getLogger().warning(f'Warning: {response.status_code}')
    except Exception as e:
        getLogger().error(f'Error: {e}')


# Get execution details using testcase id. Request and EndPoints: GET /executions/:id
def checkTestStatus(responseID):
    endpoint = "/executions/" + responseID
    pollingUrl = getUrl(endpoint)
    getLogger().info(f'Polling URL: {pollingUrl}')
    while not requests.get(url=pollingUrl, headers=getHeaderResultKey()).json()['completed']:
        getLogger().info('Test case is inprogress. Checking status at every 5 seconds')
        time.sleep(5)
    getLogger().info('Test case completed.')
    res = requests.get(url=pollingUrl, headers=getHeaderResultKey()).json()
    getLogger().info(f'Task completion: {res["completed"]} and Task success status: {res["success"]}')
    if not res['success']:
        getLogger().info('Test Failed................')
        #saveFailedSuiteResult(responseID)
        #outputLink(responseID)
        shareLink(responseID)
    else:
        getLogger().info('Test Success................')
        #saveResult(responseID)
        #saveSuiteResult(responseID)
        #saveFailedSuiteResult(responseID)
        #outputLink(responseID)
        shareLink(responseID)


# Output link that shows results. i.e: https://a.testable.io/results/responseId(8296153)
def outputLink(responseID):
    outputURL = getOutputURL(responseID)
    getLogger().info(f'Click on the link to view output for the given testcase: {outputURL}')


# Sharing this test result via a public share link. Anyone who has the link can see the results without logging in.
# link https://a.testable.io/p/:rprimspglm
# POST /executions/:executionId/share
def shareLink(responseID):
    sharableLinkEndPoint = '/executions/' + responseID + '/share'
    sharableLinkURL = getUrl(sharableLinkEndPoint)
    getLogger().info(f'sharableLinkURL: {sharableLinkURL}')

    res = requests.post(sharableLinkURL, headers=getHeaderResultKey())
    if res.status_code == 200:
        res1 = res.json()
        resKeyName = res1['name']
        getLogger().info(f'resKeyName: {resKeyName}')
        publicAccessURL = getPublicURL() + '/p/' + resKeyName
        getLogger().info(f'Click on the link to view result online (Public Link): {publicAccessURL}')
    else:
        getLogger().info(f'Error while creating public sharable URL')


# Creating test result file name.
def getTestResultFileName() -> str:
    return 'lTest_result_' + getFormattedDate() + '.json'


def saveResult(responseID):
    endPointResult = '/executions/' + responseID + '/results'
    resultURL = getUrl(endPointResult)
    getLogger().info(f'Result URL: {resultURL}')
    res = requests.get(resultURL, headers=getHeaderResultKey()).json()
    getLogger().info('Storing json results into file...')
    with open(getTestResultFileName(), 'w') as json_file:
        json.dump(res, json_file)
    getLogger().info(f'Result Saved into file: {getTestResultFileName()}')


# Download assertion results
def saveSuiteResult(responseID):
    endPointSuiteResult = '/executions/' + responseID + '/suite-results'
    suiteResultURL = getUrl(endPointSuiteResult)
    getLogger().info(f'Result URL: {suiteResultURL}')
    res = requests.get(suiteResultURL, headers=getHeaderResultKey()).json()
    getLogger().info('Storing assetion json results into file...')
    with open(getTestResultFileName(), 'w') as json_file:
        json.dump(res, json_file)
    getLogger().info(f'Result Saved into file: {getTestResultFileName()}')


# Download failed assertion results GET /executions/:execution/failed-suite-results[.csv]
def saveFailedSuiteResult(responseID):
    endPointFailedSuiteResult = '/executions/' + responseID + '/failed-suite-results'
    failedSuiteResultURL = getUrl(endPointFailedSuiteResult)
    getLogger().info(f'Result URL: {failedSuiteResultURL}')
    res = requests.get(failedSuiteResultURL, headers=getHeaderResultKey()).json()
    getLogger().info('Storing failed assetion json results into file...')
    with open(getTestResultFileName(), 'w') as json_file:
        json.dump(res, json_file)
    getLogger().info(f'Failed assertion result Saved into file: {getTestResultFileName()}')


# Script will start from here.
if __name__ == '__main__':
    endPoint_start = '/start'
    start_Test(endPoint_start)

